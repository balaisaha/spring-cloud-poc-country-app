package com.example.controller;

import com.example.pojo.Role;
import com.example.pojo.User;
import com.example.service.RoleService;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class UserController {
	@Autowired 
	UserService userService;
	
	@Autowired 
	RoleService roleService;
	
	@PostMapping("/users")
	public ResponseEntity<String> addUser(@RequestBody User user, HttpServletRequest request, HttpServletResponse response) {
		HttpStatus status = null;
		Set<Role> roles = new HashSet<Role>();
		Role r = new Role();
		
		if(user != null && user.getRoles()==null) {
			r.setRole("USER");
			roles.add(r);
			user.setRoles(roles);
		}
		
		if(user != null) {
			for(Role role : user.getRoles()) {
	            roleService.addRole(role);
	        }
			
			if(userService.addUser(user)!=null)
				return new ResponseEntity<> ("User Created. ID: " + user.getId(), status.CREATED);
			else
				return new ResponseEntity<> ("An error occured! Please try again.", status.INTERNAL_SERVER_ERROR);
		} else {
				return new ResponseEntity<> ("Invalid Role data.", status.BAD_REQUEST);
		}
	}
	
	@GetMapping(value="/users")
	public List<User> getUserList() {
		List<User> users = userService.findAllUser();
		return users;
	}
	
	@GetMapping("/users/{id}")
	public ResponseEntity<User> getUser(@PathVariable(value="id") Long id, HttpServletRequest request, HttpServletResponse response) {
		Optional<User> user = userService.getUserById(id);
		
		if(!user.isPresent()) {
			return new ResponseEntity<> (HttpStatus.NOT_FOUND);
		}
		
		User resource = user.get();
		return new ResponseEntity<User> (resource, HttpStatus.FOUND);
	}
	
	private boolean validateUser(User user) {
		// TODO Auto-generated method stub
		return true;
	}
}
