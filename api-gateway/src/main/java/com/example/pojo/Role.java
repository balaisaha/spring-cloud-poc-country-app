package com.example.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@Table(name = "Roles")
public class Role extends BaseEntity{

    /**
	 * 
	 */
	private static final long serialVersionUID = 888759393028081237L;

    @Column(name = "role")
    private String role;
}