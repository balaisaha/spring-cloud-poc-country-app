package com.example.repository;

import com.example.pojo.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource(exported = false)
public interface RoleRepository extends JpaRepository<Role, Long> {
	public Role findByRole(String role);
}