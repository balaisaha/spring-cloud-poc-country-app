package com.example.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {

        httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized Test");
    }
}

   /*
    The difference between AuthenticationEntryPoint and AuthenticationFailureHandler is that the former is used to "tell"
    unauthenticated users where to authenticate, for example, by redirecting them to a login form. The latter is used to handle bad
    login attempts.
    Your AuthenticationEntryPoint is likely not called because you're throwing an exception. It would be called if a user tries to
    access an endpoint that requires authentication and you don't throw anything. In the case where no credentials are present,
    it's enough to not authenticate the user, you don't need to throw an exception.
    If you're creating a application with JWT authentication, you probably don't want to redirect the user anywhere so you can
    just use the org.springframework.security.web.authentication.HttpStatusEntryPoint or an entry point like yours to return a
    status code.
    */