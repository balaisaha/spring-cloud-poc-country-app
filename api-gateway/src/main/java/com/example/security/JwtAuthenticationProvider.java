package com.example.security;

import com.example.pojo.JwtAuthenticationToken;
import com.example.pojo.CustomUserDetails;
import com.example.pojo.User;
import com.example.service.UserService;
import com.example.util.CustomAuthenticationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class JwtAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    private JwtValidator validator;
    
    @Autowired
    private UserService userService;
    
    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
    	User user = (User) userDetails;
    	if (!user.isActive()) {
            throw new CustomAuthenticationException("Account is not confirmed yet.");
        }
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {

        JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) usernamePasswordAuthenticationToken;
        String token = jwtAuthenticationToken.getToken();
        
        List<String> list = validator.validate(token);

        Optional<User> optionalUser = userService.getUserById(Long.parseLong(list.get(1)));
        optionalUser.orElseThrow(() -> new UsernameNotFoundException("Username not found"));
        CustomUserDetails user = optionalUser.map(CustomUserDetails::new).get();
        
        if (user == null) {
        	throw new CustomAuthenticationException("User not present");
        }

        return user;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return (JwtAuthenticationToken.class.isAssignableFrom(aClass));
    }
}