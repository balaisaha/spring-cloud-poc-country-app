package com.example.security;

import com.example.pojo.JwtAuthenticationToken;
import com.example.util.CustomAuthenticationException;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@CommonsLog
public class JwtAuthenticationTokenFilter extends AbstractAuthenticationProcessingFilter {
    public JwtAuthenticationTokenFilter() {
        super(
        		new OrRequestMatcher(
        				new AntPathRequestMatcher("/api/**")
                       // new AntPathRequestMatcher("/api/**")
                )
        );
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {

        String header = httpServletRequest.getHeader("Authorization");

        if (header == null || !header.startsWith("Bearer ")) {
            throw new CustomAuthenticationException("JWT Token is missing");
        }

        String authenticationToken = header.substring(7);

        JwtAuthenticationToken token = new JwtAuthenticationToken(authenticationToken);
        Authentication auth = getAuthenticationManager().authenticate(token);
        return auth;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        log.info("Authentication successful - Origin-IP: " + request.getRemoteAddr() + "User: " + authResult.getPrincipal().toString());
        chain.doFilter(request, response);
    }

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request,
                                              HttpServletResponse response,
                                              AuthenticationException failed)
                    throws IOException, ServletException {

        SecurityContextHolder.clearContext();

        log.info("Authentication failed - Origin-IP: " + request.getRemoteAddr() + "Message: " + failed.getMessage());

        response.sendError(HttpStatus.UNAUTHORIZED.value(),
                failed.getMessage());
    }
}