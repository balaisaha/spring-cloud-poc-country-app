package com.example.security;

import com.example.pojo.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtGenerator {

    public String generate(User user) {
    	
        Claims claims = Jwts.claims()
                			.setSubject(user.getName());
        claims.put("id", String.valueOf(user.getId()));

        int addMinuteTime = 30;
        Date issueDate = new Date(); //now
        Date expiryDate = DateUtils.addMinutes(issueDate, addMinuteTime); //add minute

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, "spring-cloud-is-awesome")
                .setIssuedAt(issueDate)
                .setExpiration(expiryDate)
                .compact();
    }
}