package com.example.security;

import com.example.util.CustomAuthenticationException;
import io.jsonwebtoken.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class JwtValidator {

    private String secret = "spring-cloud-is-awesome";

    public List<String> validate(String token) {
    	
    	List<String> list = new ArrayList<String>();
        String userID = null;
        String username = null;
        
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
            
            username = body.getSubject();
            userID = (String) body.get("id");
            list.add(username);
            list.add(userID);
        }
        catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException
                | SignatureException | IllegalArgumentException e) {
            throw new CustomAuthenticationException(e.getMessage());
        }

        return list;
    }
}