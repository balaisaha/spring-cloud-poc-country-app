package com.example.service;

import com.example.pojo.Role;
import java.util.List;
import java.util.Optional;

public interface RoleService {
	public boolean addRole(Role role);
	public Optional<Role> getRoleById(Long id);
	public List<Role> findAllRole();
	public Role getRoleByRoleName(String role);
}
