package com.example.service;

import com.example.pojo.Role;
import com.example.pojo.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
	public boolean addUser(String email, String name, String passeord, Role role);
	public Long addUser(User user);
	public Optional<User> getUserByEmail(String email);
	public Optional<User> getUserById(Long id);
	public List<User> findAllUser();
}
