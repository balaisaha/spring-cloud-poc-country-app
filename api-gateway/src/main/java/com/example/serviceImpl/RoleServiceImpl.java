package com.example.serviceImpl;

import com.example.pojo.Role;
import com.example.repository.RoleRepository;
import com.example.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService{
	@Autowired
	RoleRepository roleRepo;
	
	@Override
	public boolean addRole(Role role) {
		if(role!=null && roleRepo.save(role)!=null)
			return true;
		return false;
	}

	@Override
	public Optional<Role> getRoleById(Long id) {
		return roleRepo.findById(id);
	}
	
	@Override
	public List<Role> findAllRole() {
		return roleRepo.findAll();
	}

	@Override
	public Role getRoleByRoleName(String role) {
		return roleRepo.findByRole(role);
	}

}
