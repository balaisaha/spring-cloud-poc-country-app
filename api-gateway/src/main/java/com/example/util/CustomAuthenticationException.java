package com.example.util;
import org.springframework.security.core.AuthenticationException;

public class CustomAuthenticationException extends AuthenticationException  {
	
	private static final long serialVersionUID = -2693109705177555453L;
	
	public CustomAuthenticationException(String exception) {
		super(exception);
	}
}