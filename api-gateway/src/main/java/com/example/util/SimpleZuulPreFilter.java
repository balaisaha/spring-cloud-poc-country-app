package com.example.util;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

public class SimpleZuulPreFilter extends ZuulFilter {

  private static Logger log = LoggerFactory.getLogger(SimpleZuulPreFilter.class);

  @Override
  public String filterType() {
    return "pre";
  }

  @Override
  public int filterOrder() {
    return 1;
  }

  @Override
  public boolean shouldFilter() {
    return true;
  }

  @Override
  public Object run() {
    String credentials = "YmFsYWk6YmFsYWk=";

    RequestContext ctx = RequestContext.getCurrentContext();
    ctx.addZuulRequestHeader("Authorization", "Basic " + credentials);

    HttpServletRequest request = ctx.getRequest();

    //log.info(String.format("%s request to %s", request.getMethod(), request.getRequestURL().toString()));

    return null;
  }

}