package com.example;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

// @RefreshScope //this is used for remote config refresh
@RestController
public class ControllerCountryDetails {

    private static final Logger LOG = LoggerFactory.getLogger(ControllerCountryDetails.class);

    @Value("${name: Default Hello}")
    private String name;

    @GetMapping("/appname")
    public String name() {
        return name;
    }

    @Autowired
    CountryRepository countryRepo;

    @GetMapping("/refresh")
    @Transactional
    public @ResponseBody String getCountryDetails(){

        countryRepo.truncateTable();

        final String uri = "https://restcountries.eu/rest/v2";
        List<Country> countryList = new ArrayList<Country>();

        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);

        JSONArray jsonarray = new JSONArray(result);
        for (int i = 0; i < jsonarray.length(); i++) {
            Country country = new Country();
            JSONObject jsonobject = jsonarray.getJSONObject(i);
            String name = jsonobject.getString("name");
            String alpha2Code = jsonobject.getString("alpha2Code");
            String alpha3Code = jsonobject.getString("alpha3Code");
            String capital = jsonobject.getString("capital");
            String region = jsonobject.getString("region");
            String flag = jsonobject.getString("flag");

            JSONArray arr = jsonobject.getJSONArray("currencies");
            String currenciesCode = arr.getJSONObject(0).isNull("code") ? arr.getJSONObject(1).isNull("code") ? "Not Known" : arr.getJSONObject(1).getString("code") : arr.getJSONObject(0).getString("code");

            country.setName(name);
            country.setAlpha2Code(alpha2Code);
            country.setAlpha3Code(alpha3Code);
            country.setCapital(capital);
            country.setRegion(region);
            country.setCurrenciesCode(currenciesCode);
            country.setFlag(flag);

            countryList.add(country);
        }
        countryRepo.saveAll(countryList);
        return "Database refreshed";
    }

    @GetMapping("/{name}")
    public Country getCountry(@PathVariable String name, HttpServletRequest request,
                              HttpServletResponse response){

        for (Enumeration names = request.getHeaderNames(); names.hasMoreElements();) {
            String str = (String)names.nextElement();
            System.out.println(str);
        }

        LOG.info("getting currency details for {} in country-service",name);
        return countryRepo.findByAlpha3Code(name);
    }
}

