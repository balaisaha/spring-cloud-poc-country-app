package com.example;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {
    public Country findByAlpha3Code(String alpha3Code);

    @Modifying
    @Query(
            value = "truncate table `spring-cloud`.country",
            nativeQuery = true
    )
    void truncateTable();
}
